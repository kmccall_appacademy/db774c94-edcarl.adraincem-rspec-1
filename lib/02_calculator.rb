# Calculator

def add(firstnum, secondnum)
  firstnum + secondnum
end

def subtract(firstnum, secondnum)
  firstnum - secondnum
end

def sum(array_of_nums)
  return 0 if array_of_nums.empty?
  array_of_nums.reduce(:+)
end

def multiply(*numbers)
  numbers.reduce(:*)
end

def power(num, power)
  num**power
end

def factorial(num)
  return 1 if num == 0
  product = 1
  num.downto(1) { |number| product *= number }
  product
end
