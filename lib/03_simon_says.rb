def echo(input)
  input
end

def shout(input)
  input.upcase
end

def repeat(input, repeat_input = 2)
  repeat_str = []
  idx = 0
  while idx < repeat_input
    repeat_str << input
    idx += 1
  end
  repeat_str.join(' ')
end

def start_of_word(word, num_of_letters)
  word_str = ""
  word.each_char do |ch|
    word_str << ch
    break if word_str.length == num_of_letters
  end
  word_str
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(title)
  little_word = ['and', 'over', 'the']
  cap_title = []
  title.split.each_with_index do |word, idx|
    unless little_word.include?(word) && idx > 0
      cap_title << word.capitalize
      next
    end
    cap_title << word
  end
  cap_title.join(' ')
end
