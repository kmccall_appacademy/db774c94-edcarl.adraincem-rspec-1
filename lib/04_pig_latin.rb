def translate(words)
  words_arr = words.split
  punctuation = ""
  idx = 0

  while idx < words_arr.length
    word_char = words_arr[idx].chars
    punctuation = word_char.pop if punctuation?(words_arr[idx])
    until vowels?(word_char.first) && word_char.last != 'q'
      word_char << word_char.shift.downcase
    end
    words_arr[idx] = pig_latin(words_arr[idx], word_char) + punctuation
    idx += 1
  end

  words_arr.join(' ')
end

def vowels?(letter)
  vowels = "aeiou"
  vowels.include?(letter)
end

def punctuation?(word)
  punctuations = "!?.:;,"
  word.each_char { |ch| return true if punctuations.include?(ch) }
  false
end

def pig_latin(word, chars_word)
  if word.chars.first == word.chars.first.capitalize
    "#{chars_word.join.capitalize}ay"
  else
    "#{chars_word.join}ay"
  end
end
